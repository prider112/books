const express = require('express');

const router = express.Router();
module.exports = router;


const bookController = require('../controllers/bookcontroller');

router.get('/', (req, res) => {
    res.render('index')
});

router.get('/add', (req, res) => {
    res.render('book-options/new-book')
});

// Write
router.post('/new-book', bookController.bookCreate);
// Read
router.get('/book-catalog', bookController.bookRead);
// Update
router.get('/edit/:id', bookController.editForm);
router.post('/edit-book/:id', bookController.bookUpdate);
// Delete
router.post('/delete/:id', bookController.bookDelete);
const mongoose = require('mongoose');

const {Schema} = mongoose;

const BookSchema = new Schema({
    title: {type: String, required: true, max: 100},
    quote: {type: String, required: true, max:100},
    genere: {type: String, required: true, max:30}
});


// Exportamos el modelo
module.exports = mongoose.model('Book', BookSchema);
const Book = require('../models/bookmodel');


/*
 * Summary. 
 bookcontroller.js defines the functions necesary to create a full CRUD regarding a book collection.
 */

/*
* Create function
 
*/

exports.bookCreate = function(req, res) {
    const { title, quote, genere } = req.body;
    const book = new Book({ title, quote, genere });
    book.save();
    res.redirect("/");
};

/*
 * Read function

 */
exports.bookRead = function(req, res) {
    Book.find({}, function(err, book) {
        res.render("book-options/list-books", { book });

    });
};

/* Summary.
 * bookUpdate and editForm are elements of the Update portion of the crud
 * editForm seraches by id the book to be edited by bookUpdate.
 */

/* Update function

*/
exports.bookUpdate = function(req, res, next) {
    Book.findByIdAndUpdate(req.params.id, { $set: req.body }, function(err) {
        if (err) return next(err);
        res.redirect("/books/book-catalog");
    });
};

/*

*/

exports.editForm = function(req, res) {
    Book.findById(req.params.id, function(err, book) {
        res.render("book-options/update-book", { book });

    });
};


/*
 * Delete function
 */
exports.bookDelete = function(req, res, next) {
    Book.findByIdAndRemove(req.params.id, function(err) {
        if (err) return next(err);
        res.redirect("/books/book-catalog");
    })
};
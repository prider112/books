// Se establece todo lo relativo al servidor

// Configuracion
const methodOverride = require('method-override');
const session = require('express-session');
const path = require('path');
const cors = require('cors');

// Express
const express = require('express')
const exphbs = require('express-handlebars');

const app = express();
// require('./config/passport');

// Body parser
const bodyParser = require('body-parser');
const book = require('./routes/bookroute');

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use('/books', book);

// Configuracion de las vistas
app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    layoutsDir: path.join(__dirname, 'views', 'layout'),
    partialsDir: path.join(__dirname, 'views', 'partials'),
    extname: '.hbs'
}));
app.set('view engine', '.hbs');

// Middleware
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(methodOverride('_method'));
app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}));

// etc


module.exports = app;
require('dotenv').config();
const config = require('./config/config')


const app = require('./server');
require('./database');

app.listen(config.port, () => {
    console.log('App is running');
});
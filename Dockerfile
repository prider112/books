FROM node:12.16.0-alpine3.9 AS builder

WORKDIR /books
COPY package.json .

COPY . .

RUN npm install
RUN npm install -g pkg
RUN pkg -t node6-alpine-x64 .

FROM alpine
RUN  apk add --update --no-cache \ 
        libstdc++ \
        libgcc
COPY --from=builder /books/books .
CMD ./books

